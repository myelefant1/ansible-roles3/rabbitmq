## Rabbitmq

This roles installs rabbitmq with a default configuration on debian systems.

## Role parameters

| name                      | value       | default_value   | description             |
| --------------------------|-------------|-----------------|-------------------------|
| rabbitmq_started          | boolean     | true            | starts rabbitmq         |
| rabbitmq_update_packages  | boolean     | true            | update packages cache   |


## Using this role

### ansible galaxy
Add the following in your *requirements.yml*.
```yaml
---
- src: https://gitlab.com/ansible-roles3/rabbitmq.git
  scm: git
  version: v1.0
```

### Sample playbook

```yaml
---
- hosts: all
  roles:
      - role: rabbitmq
        rabbitmq_started: false
        rabbitmq_update_packages: true
```
